# 36 Hours Rest every 7 days

Solving How to monitor ALPA 36 Hours Rest in 7 days rostering problem.

Excerpt from the ALPA contract as below:
 
![36-7 rule](36-7.jpg "36/7")

*I need to make a logical way of monitoring this 36 hour break as if it was a rule in a rostering system.*

## My basic assumptions and rules I will use:

- 36 hour rest period must include 2 consecutive nights rest of 8 hours between 10pm - 8am.

- I will make the assumption for the sake of ease of use that there will be 2 shifts being worked 6am-2pm (morning) & 2pm-10pm (evening).

- I also will create a rest shift which will be a generic assumption of 10pm - 6am for appropriate sleep period.

- I will not factor in travel time or sign-in/ sign-off time.

- My interpretation of the rule is that the consecutive days starts immediately following the last 36 hour rest period.

- The wording of once in every seven consecutive day period indicates that this 36 hour break could be at any day in this period although it must start midday on the sixth day at the latest.  

- My assumption is that the most efficient use of this rule is to try and schedule the 36 hour rest period at the last possible point to have the least amount of 36 hour rest periods possible. This maximises productive work hours.

- The biggest draw back from scheduling the 36 hour rest period on the last possible point is that the pilot is only available for a half shift on the sixth day(must start break at midday). Also the fact that rest is extended to 6am on the 8th day resulting in a 42 hour break.

- Potential efficiency could be made by starting the break at 10pm on the 5th day, having the break end at 10am on the 7th day.

## Strategy to implement rules

Preferably I will develop this as a TDD rule set. Have my hard rules be created as tests and then an indication of an appropriate roster that complies with the 36 hour break rules, will pass all the tests. Initially the plan would be to create a passing roster. Then create the most time efficient roster with pilot productivity maximised and rest minimised within the test environment.

I will create a mock up roster of 28 days, this will most likely be an array of 28 objects with 3 properties - morningShift: true/false, eveningShift: true/false, rest:true/false.

The benefits of the array is that I can loop/map over the array to apply functional rules to each object that is applicable.

The idea is to create a ticker function or day tracker that for every object it will increase its count by 1, effectively tracking days, it eill count until it comes across a 36 hours or until 7 days. If no rest day is present at 7 days then it will console log an error with the roster. After a 36 hour rest it will reset the day counter. The main purpose of this function is to track the 7 day part of the rule.

Another function will apply the rest day inside the roster if it is not present.

**Initial Plan** 

- [ ] Develop main rules for TDD Tests

- [ ] Pass tests with basic logic and functions

- [ ] Create array of objects with properties

- [ ] Apply logic and functions to get my initial roster design to pass

- [ ] Refactor & create most efficient roster in the test environment

- [ ] Attempt to write in Python


