const rostering = require('./rostering.js')

test('Jest is working', () => {
  expect(true).toBeTruthy()
})

test('roster is 7 days long', () => {
  const roster = rostering.rosterCheck()

  expect(roster).toHaveLength(7)
})

// test('roster to have 5 shifts every 7 days', () => {
//   const roster = rostering.createRosterDays()

//   expect(roster).toHaveLength(28)
// })
